# "Dancing Goats Cafe" Interview Task

Dancing Goat Cafe is the name of a **fictitious** coffee company. This task is a simulation of a request from a customer to 
implement the cafe list page on their React site. You are to finish implementing the site and make some corrections. This React application is the front-end to a Kentico CMS site. The integration with Kentico Kontent (the headless-CMS system) has been implemented already (it's a demo CMS provided by Kentico for training purposes), so no specific knowledge is required. The application needs completing and fixing.

The design for the site is in Figma here: https://www.figma.com/file/XtTabGOwMCygnXXIWVE2Zd/FE-Interview-Dancing-Goat?node-id=0%3A1

## Building and running the code locally
Use Node Version 14.15.0

Download, or use git clone.

Download dependencies by navigating to the frontendinterviewtask folder in a terminal and running `yarn`

Once that is finished, start the dev server locally using `yarn start`

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## Task
There are 2 parts to the task, which shouldn't take more than about an hour to complete. Feel free to fork the repo, or clone & host it elsewhere with your fixes.

#### Part 1
Implement a component to display the individual cafes. It should match the design in Figma. **The site needs to be responsive**, ie the layout of the site should adapt to the width of the display. You *may* need to modify other components to achieve this.

If you're unable to match the design due to the data held in the CMS, please do the best you can in a way that you would for a real customer (ie production-ready code); explain your thought process/next course of action.

Note: You don't have to use the 3rd-party material-ui library, if it is easier for you not to. Use whatever techniques you find easiest, or more appropriate for the task.

#### Part 2 
Fix 2 bugs:

* There is some junk data being returned from the API: cafes without complete address information. These need to be filtered/pruned. **Please find the most appropriate place to do this**. Also, find an appropriate property to use to filter the unwanted records. There are approximately 6 real records, and the rest are junk. This should ideally be done in the CMS, but the client has asked this to be done in the front-end.
* The footer bar doesn't match the design. It needs to be sticky, and the layout isn't quite right.

#### Optional Improvements
If you have any time remaining, see if there are any further improvements you could make to the site, or things that could be added or fixed. For example, use the GoogleMap dummy component to display a map to a cafe, add animation, or improve the styling of the site.


#### Important
This is intended to be an open-ended test project, which you should be able to mostly complete within the time. Do not worry if you are unable to complete it fully, as code quality, attention to detail, and the ability to explain your thoughts and reasoning are more important than raw speed. 